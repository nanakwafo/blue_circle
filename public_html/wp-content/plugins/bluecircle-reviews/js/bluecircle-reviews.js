jQuery(document).ready(function($) {
    $(document).on('submit',"[name='review']",function(e){

        e.preventDefault();

        var name         = $("[name='name']").val();
        var emailAddress = $("[name='email_address']").val();
        var product      = $("[name='review']").data('product');
        var title        = $("[name='title']").val();
        var rating       = $("[name='rating']").val();
        var message      = $("[name='message']").val();

        $.ajax({
            method: "POST",
            url: "/wp-json/reviews/add-review",
            data: { name: name, email_address: emailAddress, product: product, title: title, rating: rating, message: message },
            async: false
        })
        .success(function() {

            alert('success');
        })
        .error(function() {
            e.preventDefault();
        });
    });
});