<?php
/*
Plugin Name: Bluecircle reviews
Version: 1.0.0
*/
$loader = require __DIR__ . '/vendor/autoload.php';
$loader->addPsr4('BlueCircleReviews\\Services\\', __DIR__ . '/services');

add_action( 'init', 'create_post_type');
function create_post_type() {
    
    add_theme_support('post-thumbnails');
    $postTypes = array('Reviews' => array('label' => 'Reviews', 'exclude_from_search' => false),
    );
    
    foreach($postTypes as $postType => $postTypeDetails)
    {
        $postTypeName = strtolower($postType);
        add_post_type_support( $postTypeName, 'thumbnail' );
        
        register_post_type($postTypeName,
            array(
                'labels' => array(
                    'name' => $postTypeDetails['label'],
                    'singular_name' => __($postType),
                ),
                'public' => true,
                'has_archive' => true,
                'taxonomies'  => array( 'category' ),
                'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
                'exclude_from_search' => $postTypeDetails['exclude_from_search'],
            )
        );
    }
}

$reviewsService = new \BlueCircleReviews\Services\Reviews(new \Mustache_Engine());

//rest endpoints
add_action( 'rest_api_init', function () use ($reviewsService) {
    register_rest_route('reviews', 'add-review', array(
        'methods' => 'POST',
        'callback' => array($reviewsService, 'addReview')
    ));
});
add_action('init', function() {
    //add javascript
    wp_register_script( 'bluecircle-reviews', plugin_dir_url( __FILE__ ).'js/bluecircle-reviews.js');
    wp_enqueue_script('bluecircle-reviews', plugin_dir_url( __FILE__ ).'js/bluecircle-reviews.js', array('jquery'), 1.0, true);
    wp_localize_script( 'bluecircle-reviews', 'bluecircle-reviews', array('user' => wp_get_current_user()));
    
    new \BlueCircleReviews\Services\Reviews(new \Mustache_Engine());
});
