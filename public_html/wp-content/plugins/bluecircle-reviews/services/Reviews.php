<?php
namespace BlueCircleReviews\Services;
/**
 * Reviews Service.
 */
class Reviews
{
    /**
     * Mustache.
     *
     * @var \Mustache_Engine
     */
    private $_mustache;
    
    /**
     * Reviews constructor.
     *
     * @param \Mustache_Engine $mustache
     */
    public function __construct(\Mustache_Engine $mustache)
    {
        $this->_mustache = $mustache;
        
        add_shortcode( 'get_product_reviews', array($this, 'getReviews'));
        add_shortcode( 'get_all_reviews', array($this, 'getAllReviews'));
        add_shortcode( 'get_average_rating', array($this, 'getAverageRating'));
    }
    
    /**
     * Add Review.
     *
     * @param $args
     *
     * @return bool
     */
    public function addReview($args)
    {
        $postId = wp_insert_post(array('post_type' => 'reviews',
                                        'post_title'  => sprintf('%s-%s-%s', $args['product'], $args['name'], $args['email_address']),
                                        'post_status' => 'publish',
                                        'post_content' => ' ')
                                        );
    
        update_field('name', $args['name'], $postId);
        update_field('email_address', $args['email_address'], $postId);
        update_field('product', $args['product'], $postId);
        update_field('title', $args['title'], $postId);
        update_field('message', $args['message'], $postId);
        update_field('rating', $args['rating'], $postId);
        update_field('approved', 0, $postId);
        
        return true;
    }
    
    /**
     * Get All Reviews.
     *
     * @param $args
     */
    public function getAllReviews($args)
    {
        $reviewArgs = array('posts_per_page'	=> array_key_exists('number_of_posts', $args) ? $args['number_of_posts'] : 5,
            'post_type'		=> 'reviews',
            'post_status'  => 'publish',
            'meta_query'	=> array('relation'		=> 'AND',
                array(
                    'key'		=> 'approved',
                    'value'		=> '1',
                    'compare'	=> '='
                ),
                array(
                    'key'		=> 'product',
                    'value'		=> $args['product'],
                    'compare'	=> '='
                )
            )
        );
        
        $reviews = new \WP_Query($reviewArgs);
        $posts = $reviews->posts;
        
        $template = file_get_contents(sprintf('%s/../../bluecircle-reviews/templates/all_listing.mustache', plugin_dir_path( __FILE__ )));
        
        $reviewPosts = [];
        
        foreach($posts as $post)
        {
            $reviewPost['name']          = get_post_meta($post->ID, 'name', true);
            $reviewPost['email_address'] = get_post_meta($post->ID, 'email_address', true);
            $reviewPost['product']       = get_post_meta($post->ID, 'product', true);
            $reviewPost['title']         = get_post_meta($post->ID, 'title', true);
            $reviewPost['message']       = get_post_meta($post->ID, 'message', true);
            $reviewPost['rating']        = get_post_meta($post->ID, 'rating', true);
            $reviewPost['rating_image']  = sprintf(plugins_url('../images/rating-%s.png', __FILE__), get_post_meta($post->ID, 'rating', true));
            $reviewPost['product_image'] = sprintf(plugins_url('../images/product-%s.png', __FILE__), get_post_meta($post->ID, 'product', true));
            $reviewPost['date']          = mysql2date('d M Y', $post->post_date);
            
            $reviewPosts[] = $reviewPost;
        }
        
        echo $this->_mustache->render($template, array('reviews' => $reviewPosts));
    }
    
    /**
     * Get Reviews.
     *
     * @param $args
     */
    public function getReviews($args)
    {
        $reviewArgs = array('posts_per_page'	=> array_key_exists('number_of_posts', $args) ? $args['number_of_posts'] : 5,
            'post_type'		=> 'reviews',
            'post_status'  => 'publish',
            'meta_query'	=> array('relation'		=> 'AND',
                array(
                    'key'		=> 'approved',
                    'value'		=> '1',
                    'compare'	=> '='
                ),
                array(
                    'key'		=> 'product',
                    'value'		=> $args['product'],
                    'compare'	=> '='
                )
            )
        );
        $reviews = new \WP_Query($reviewArgs);
        $posts = $reviews->posts;
    
        $template = file_get_contents(sprintf('%s/../../bluecircle-reviews/templates/listing.mustache', plugin_dir_path( __FILE__ )));
        
        $reviewPosts = [];
        
        foreach($posts as $post)
        {
            $reviewPost['name']          = get_post_meta($post->ID, 'name', true);
            $reviewPost['email_address'] = get_post_meta($post->ID, 'email_address', true);
            $reviewPost['product']       = get_post_meta($post->ID, 'product', true);
            $reviewPost['title']         = get_post_meta($post->ID, 'title', true);
            $reviewPost['message']       = get_post_meta($post->ID, 'message', true);
            $reviewPost['rating']        = get_post_meta($post->ID, 'rating', true);
            $reviewPost['rating_image']  = sprintf(plugins_url('../images/rating-%s.png', __FILE__), get_post_meta($post->ID, 'rating', true));
            $reviewPost['product_image'] = sprintf(plugins_url('../images/product-%s.png', __FILE__), get_post_meta($post->ID, 'product', true));
            $reviewPost['date']          = mysql2date('d M Y', $post->post_date);
            
            $reviewPosts[] = $reviewPost;
        }
        
        echo $this->_mustache->render($template, array('reviews' => $reviewPosts));
    }
    
    /**
     * Get Average Rating.
     *
     * @param $args
     *
     * @return int
     */
    public function getAverageRating($args)
    {
        $sql = sprintf("SELECT AVG(m2.meta_value) AS avg_rating
                                FROM wp_posts p
                                INNER JOIN wp_postmeta m ON p.id = m.post_id  && m.meta_key = 'product' && m.meta_value = '%s'
                                INNER JOIN wp_postmeta m2 ON m2.post_id = m.post_id  && m2.meta_key = 'rating'
                                WHERE p.post_type = 'reviews' && p.post_status = 'publish'", $args['product']);
        
        global $wpdb;
    
        $results = $wpdb->get_row($sql);
        
        $avgRating = ceil($results->avg_rating) ?? 0;
    
        $template = file_get_contents(sprintf('%s/../../bluecircle-reviews/templates/average_rating.mustache', plugin_dir_path( __FILE__ )));
    
        $avgRatingImage = sprintf(plugins_url('../images/rating-%s.png', __FILE__), $avgRating, true);
        
        $params['avg_rating_image'] = $avgRatingImage;
        
        if($avgRating > 0)
            $params['avg_rating'] = $avgRating;
        
        echo $this->_mustache->render($template, $params);
    }
}