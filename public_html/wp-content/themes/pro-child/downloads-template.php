<?php
/*
Template Name: Downloads Page

*/

get_header(); ?>

<div class="downloads-head">

<h1>Downloads</h1>

</div>

<div class="downloads-container">

<div class="downloads-col">
<h2 style="color:#9EA0A6 !important;">Downloads</h2>
<h2>Featured</h2>

<?php 
$args = array( 
'post_type' => 'download_doc',
'category_name' => 'docs-featured',
'posts_per_page' => 2,
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>
           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>

</div>

<div class="downloads-col">
<h2 style="color:#9EA0A6 !important;">Downloads</h2>
<h2>Recently added</h2>

<?php 
$args = array( 
'post_type' => 'download_doc',
'posts_per_page' => 2,
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>
           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>

</div>
</div>

<div class="downloads-main-container">

<h2 style="color:#9EA0A6 !important; font-size:30px !important">Blue Circle</h2>
<h2 style="font-size:30px !important">All Downloads</h2>
    
<div class="downloads-inner-container">

    
    
    
                    <div class="search-wrap search-accordian-active">
					
      
                    <header class="entry-header">
                      <h2>Product Datasheets - Packed</h2>
                    </header>
                    <div class="search-content">
                    
                    

<?php 
$args = array( 
'post_type' => 'download_doc',
'category_name' => 'docs-packed',
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>


<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><p class="download-desc"><?php $doc_desc = get_field('doc_desc'); echo $doc_desc; ?></p></a>

           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>
                    
                    
    </div>
    </div>
    
    
    
    <div class="search-wrap">
					
      
                    <header class="entry-header">
                      <h2>Product Datasheets - Bulk</h2>
                    </header>
                    <div class="search-content">
                    
                    

<?php 
$args = array( 
'post_type' => 'download_doc',
'category_name' => 'docs-bulk',
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>


<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><p class="download-desc"><?php $doc_desc = get_field('doc_desc'); echo $doc_desc; ?></p></a>

           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>
                    
                    
    </div>
    </div>
                    
         <div class="search-wrap">
					
      
                    <header class="entry-header">
                      <h2>Material Safety Data Sheets</h2>
                    </header>
                    <div class="search-content">
                    
                    

<?php 
$args = array( 
'post_type' => 'download_doc',
'category_name' => 'docs-material',
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>


<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><p class="download-desc"><?php $doc_desc = get_field('doc_desc'); echo $doc_desc; ?></p></a>

           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>
                    
                    
    </div>
    </div>  
    
    <div class="search-wrap">
					
      
                    <header class="entry-header">
                      <h2>Declared Values</h2>
                    </header>
                    <div class="search-content">
                    
                    

<?php 
$args = array( 
'post_type' => 'download_doc',
'category_name' => 'docs-declared',
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>


<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><p class="download-desc"><?php $doc_desc = get_field('doc_desc'); echo $doc_desc; ?></p></a>

           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>
                    
                    
    </div>
    </div>  
    
    
    <div class="search-wrap">
					
      
                    <header class="entry-header">
                      <h2>Certification</h2>
                    </header>
                    <div class="search-content">
                    
                    

<?php 
$args = array( 
'post_type' => 'download_doc',
'category_name' => 'docs-certification',
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>


<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><p class="download-desc"><?php $doc_desc = get_field('doc_desc'); echo $doc_desc; ?></p></a>

           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>
                    
                    
    </div>
    </div>  
    
    
      <div class="search-wrap">
					
      
                    <header class="entry-header">
                      <h2>Fly Ash Reports</h2>
                    </header>
                    <div class="search-content">
                    
                    

<?php 
$args = array( 
'post_type' => 'download_doc',
'category_name' => 'docs-fly-ash',
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>


<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><p class="download-desc"><?php $doc_desc = get_field('doc_desc'); echo $doc_desc; ?></p></a>

           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>
                    
                    
    </div>
    </div>  
    
   <div class="search-wrap">
					
      
                    <header class="entry-header">
                      <h2>Technical Information</h2>
                    </header>
                    <div class="search-content">
                    
                    

<?php 
$args = array( 
'post_type' => 'download_doc',
'category_name' => 'docs-technical',
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>


<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><p class="download-desc"><?php $doc_desc = get_field('doc_desc'); echo $doc_desc; ?></p></a>

           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>
                    
                    
    </div>
    </div>    
    
    <div class="search-wrap">
					
      
                    <header class="entry-header">
                      <h2>Application Guides</h2>
                    </header>
                    <div class="search-content">
                    
                    

<?php 
$args = array( 
'post_type' => 'download_doc',
'category_name' => 'docs-application',
'post_status' => 'publish',
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="download-wrap">
						
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><h2 class="download-title"><?php the_title(); ?></h2></a>


<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>"><p class="download-desc"><?php $doc_desc = get_field('doc_desc'); echo $doc_desc; ?></p></a>

           
<a href="<?php $doc_download = get_field('doc_url'); echo $doc_download; ?>">
<img src="<?php $image = get_field('thumbnail_image'); ?><?php echo $image['url']; ?>" alt="download-thumbnail" />
</a>

</div>

<?php endwhile; else: ?> <p>Sorry, there's nothing to show here</p> <?php endif; ?>
<?php wp_reset_query(); ?>
                    
                    
    </div>
    </div>    
    
    
    </div>
</div>







<script>

jQuery(document).on('ready ajaxComplete', function () {

jQuery('.search-wrap').on('click', event =>{
    
  jQuery('.search-wrap').removeClass('search-accordian-active');
  jQuery(event.currentTarget).addClass('search-accordian-active');
  
});
    
    });
</script>


<?php get_footer(); ?>