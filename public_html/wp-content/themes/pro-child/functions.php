<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to Pro in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );
include( get_theme_file_path('/includes/front/enqueue.php') );

add_action('wp_enqueue_scripts', 'telp_enqueue', 1000);


// Additional Functions
// =============================================================================

function my_wpdiscuz_shortcode() {
  if (file_exists(ABSPATH . 'wp-content/plugins/wpdiscuz/templates/comment/comment-form.php')) {
      ob_start();
      include_once ABSPATH . 'wp-content/plugins/wpdiscuz/templates/comment/comment-form.php';
      return ob_get_clean();
  }
}
add_shortcode('wpdiscuz_comments', 'my_wpdiscuz_shortcode');