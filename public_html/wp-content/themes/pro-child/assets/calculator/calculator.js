jQuery(document).ready(function($) {

    var calculatorType = $('#calc-outer').data('type');
    var mixtureType    = $('.calc-option.tab-active').data('mixture-type');

    $("[name='length'], [name='depth'], [name='width']").change(function() {
        generateCalculator();
    });

    $('.calc-option').click(function() {
        updateCalculatorFields();
        generateCalculator();
    });

    function updateCalculatorFields()
    {
        var mixtureType = $('.calc-option.tab-active').data('mixture-type');
        var depth       = $("[name='depth']").closest('.calc-col');

        switch (calculatorType) {
            case "mastercrete":
            case "gpc":
            case "procem":
            case "extra_rapid_cement":
            case "sulfacrete":
                if(mixtureType == 'mortar_1_6')
                {
                    depth.hide();
                }
                else
                {
                    depth.show();
                }

            break;
            case "snowcrete":
            case "postcrete":
            case "qam":
            case "slablayer":
            case "high_strength_concrete":
            case "mpc":
            case "quickset_concrete":

                break;
        }
    }

    function generateCalculator()
    {
        var result      = "";

        var length      = $("[name='length']").val();
        var depth       = $("[name='depth']").val();
        var width       = $("[name='width']").val();
        var mixtureType = $('.calc-option.tab-active').data('mixture-type');

        switch (calculatorType) {
            case "gpc":
            case "mastercrete":
            case "procem":
            case "extra_rapid_cement":
            case "sulfacrete":
                result = getMastercrete(mixtureType, length, width, depth);
                break;
            case "snowcrete":
                result = getSnowcrete(mixtureType, length, width, depth);
                break;
            case "postcrete":
                result = getPostcrete(length, width, depth);
                break;
            case "qam":
                result = getQam(mixtureType, length, width);
                break;
            case "slablayer":
                result = getSlablayer(length, width);
                break;
            case "high_strength_concrete":
            case "mpc":
            case "quickset_concrete":
                result = getHighStrengthConcrete(length, width, depth);
                break;
            case "hydralime":
                result = getHydralime(length, width);
                break;


            // case "repair_concrete":
            //     result = displayRepairConncrete();
            //     break;
            default:
                console.log("No product was selected!");

        }

        $('.calc-results-new').attr("style", "display: inline-block !important").html(result);
    }

    function getMastercrete(mixtureType, length, width, depth) {

        var bagsNo = "";
        var sandBallast = 0;
        var area;
        var volume;
        var materialType = "";
        if (mixtureType === "concrete_1_4"){

            // Calculate volume in cubic meters
            volume = (length * width * depth) / 1000000;

            // Calculate the number of cement bags, each bag is 25Kg.
            bagsNo = volume * 14;

            if (bagsNo <= 0.5){
                bagsNo = "1 Handy pack of 12.5Kg";
            } else {
                bagsNo = Math.ceil(bagsNo);
            }

            // Calculate the amount of Ballast in Kg
            sandBallast = (volume * 1850).toFixed(2);

            materialType = "Ballast";
        } else {
            // Calculate area in square meters
            area = (length * width) / 10000;
            bagsNo = area * 0.3929;

            if (bagsNo <= 0.5){
                bagsNo = "1 Handy pack of 12.5Kg";
            } else {
                bagsNo = Math.ceil(bagsNo);
            }
            // bagsNo = area * 0.3929;
            sandBallast = (area * 53.04).toFixed(2);
            materialType = "Sand";
        }

        if (bagsNo <= 0.5){
            bagsNo = "1 Handy Pack of 12.5Kg";
        }

        return length > 0 || width > 0 || depth > 0 ? "<h3>Number of bags of cement:</h3>"+ bagsNo+ "<h3>Amount of "+materialType + " in KG:</h3>"+ sandBallast+ "<h4>Add water till desired consistency.</h4>" : "";
    }

    function getSnowcrete(mixtureType, areaLength, areaWidth)
    {
        var bagsNo;
        var sand;
        var area;
        var volume;
        if (mixtureType === "render"){
            // Calculate volume in cubic meters
            volume = (areaLength * areaWidth) * 1 / 1000000;

            // Calculate the number of cement bags
            bagsNo = (volume * 10) *25 + "kg";

            // Calculate the amount of Ballast in Kg
            sand = volume * 10 * 25 * 10.8;

        } else {
            // Calculate area in square meters

            area = (areaLength * areaWidth) / 10000;
            // bagsNo = Math.ceil(area * 0.3929);
            bagsNo = area * 0.3929;
            if (bagsNo <= 0.5){
                bagsNo = "Handy pack of 12.5Kg";
            } else {
                bagsNo = Math.ceil(bagsNo);
            }
            sand = area * 53.04;

        }

        return areaLength > 0 || areaWidth > 0 ? "<h3>Number of bags of cement:</h3>"+ bagsNo+ "<h3>Amount of Sand in KG:</h3>"+ Math.round(sand)+ "<h4>Add water till desired consistency.</h4>" : "";
    }

    function getPostcrete(postLength, postWidth, numberOfPosts)
    {
        var volumeOfCement;
        var postcreteBagsNo;
        var litersOfWater;

        volumeOfCement = postLength * 0.25 * postWidth * postWidth * 9 / 1000000;

        postcreteBagsNo = ((volumeOfCement - postLength * 0.25 * postWidth * postWidth / 1000000) * numberOfPosts / 0.012) * 0.5;

        if (postcreteBagsNo <= 0.5) {
            postcreteBagsNo = "Handy pack of 12.5Kg";
        } else {
            postcreteBagsNo = postcreteBagsNo.toFixed(1);
        }

        // postcreteBagsNo = (postLength * 0.25 * postWidth * postWidth * 9 / 1000000 - postLength * 0.25 * postWidth * postWidth) * numberOfPosts / 0.012 * 0.5;
        // litersOfWater = (postLength*0.25*postWidth*postWidth*9/1000000-postLength*0.25*postWidth*postWidth)*numberOfPosts/6*1000;
        litersOfWater = (volumeOfCement - postLength * 0.25 * postWidth * postWidth / 1000000) / 6 * 1000;
        litersOfWater = litersOfWater.toFixed(1);

        return postLength > 0 || postWidth > 0 || numberOfPosts ? "<h3>Number of bags of postcrete needed:</h3>" + postcreteBagsNo +
            "<h3>Litres of water per hole (approx 1/3 of the hole):</h3>" + litersOfWater : "";
    }

    function getQam(brickType, length, width)
    {
        var metersWithOneBag, numberOfBags, numberOfBricks;
        var areaInSquareMeters = width * length;

        if (brickType === "single_skin") {
            numberOfBricks = length * width *64;
            numberOfBags = Math.ceil(numberOfBricks / 1750 * 75);

            return length > 0 || width > 0  ? "<h4>Number of bricks:</h4>" + Math.ceil(numberOfBricks) +
                "<h4>Number of bags:</h4>" + numberOfBags +
                "<h4>Total area:</h4>" + areaInSquareMeters : "";

        } else {

            width = length;
            var depth = width;

            var totalArea = length * width;
            totalArea = totalArea.toFixed(2);
            metersWithOneBag = ((1/75)/((width/100)*(depth/100)*(0.01)*100)).toFixed(2);

            return length > 0 || width > 0  ? "<h3>Total area:</h3>" + totalArea +
                "<h3>Meters with 1 bag:</h3>" + metersWithOneBag : "";
        }
    }

    function getSlablayer(length, width)
    {
        var totalArea = length * width;
        totalArea = totalArea.toFixed(2);

        var numberOfBags = totalArea / 0.4225;
        numberOfBags = Math.ceil(numberOfBags);

        return length > 0 || width > 0  ? "<h4>Total area in square meters:</h4>" + totalArea +
            "<h4>Number of 20Kg bags:</h4>" + numberOfBags +
            "<h4>You will also need mortar for the joints</h4>" : "";
    }

    function getHighStrengthConcrete(length, width, depth)
    {
        var totalVolume = length * width * depth / 1000000;
        var numberOfBags = totalVolume / 0.01 * 1.05;

        if(numberOfBags <= 0.5){
            numberOfBags = "Handy pack of 12.5Kg";
        } else{

            numberOfBags = Math.ceil(numberOfBags);
        }

        return length > 0 || width > 0 || depth > 0 ? "<h3>Cubic meters of concrete required:</h3>" + totalVolume +
            "<h3>Number of bags:</h3>" + numberOfBags : "";
    }

    function getHydralime(length, width)
    {
        var totalArea = length * width / 10000;
        var cementBags = (totalArea * 0.3929).toFixed(1);
        var sandBags = (totalArea * 53.04).toFixed(1);
        var hydraBags = (totalArea * 0.1429).toFixed(1);

        alert("<h4>Number of bags of cement:</h4>" + cementBags +
        "<h4>Number of bags sand:</h4>" + sandBags +
        "<h4>You will also need mortar for the joints</h4>");

        return length > 0 || width > 0  ? "<h4>Number of bags of cement:</h4>" + cementBags +
            "<h4>Number of bags sand:</h4>" + sandBags +
            "<h4>You will also need mortar for the joints</h4>" : "";
    }

    //////

    // var mixtureType     = jQuery("#mixture-type option:selected").val();
    // var product              = jQuery("#product option:selected").val();
    // var snowcreteMixtureType = jQuery("#mixture-snowcrete option:selected").val();
    // var brickType            = jQuery("#brick-type option:selected").val();
    //
    // //Update mixture type every time selection changes
    // jQuery("#mixture-type").change(function() {
    //         // jQuery(".calc-results").hide();
    //         jQuery(".calc-results").css('visibility', 'hidden');
    //         mixtureType = jQuery("#mixture-type option:selected").val();
    //         if (mixtureType !== "Concrete 1:4") {
    //             jQuery("#darea-container").hide();
    //         } else {
    //             jQuery("#darea-container").show();
    //         }
    //     }
    // );
    //
    // //Update mixture type every time selection changes
    // jQuery("#mixture-snowcrete").change(function() {
    //         jQuery(".calc-results").css('visibility', 'hidden');
    //         snowcreteMixtureType = jQuery("#mixture-snowcrete option:selected").val();
    //
    //     }
    // );
    //
    // jQuery("#brick-type").change(function() {
    //
    //         jQuery(".calc-results").css('visibility', 'hidden');
    //         brickType = jQuery("#brick-type option:selected").val();
    //         if (brickType === "Single skin") {
    //
    //             jQuery(".repointing-input").hide();
    //             jQuery(".single-skin-input").show();
    //         } else {
    //
    //             jQuery(".single-skin-input").hide();
    //             jQuery(".repointing-input").show();
    //         }
    //     }
    // );
    //
    //
    // jQuery("#product").change(function(){
    //     // jQuery(".calc-results").hide();
    //     jQuery(".calc-results").css('visibility', 'hidden');
    //     product =  jQuery("#product option:selected").val();
    //     mixtureType = jQuery("#mixture-type option:selected").val();
    //
    //     if (product === "Snowcrete" || mixtureType !== "Concrete 1:4") {
    //         jQuery("#darea-container").hide();
    //     } else {
    //         jQuery("#darea-container").show();
    //     }
    //
    //
    // });

    // jQuery("#calc-btn").click(function(){
    //
    //
    //     switch (product) {
    //
    //         case "Mastercrete":
    //             mastercrete();
    //             break;
    //         case "GPC":
    //             mastercrete();
    //             break;
    //         case "Procem":
    //             mastercrete();
    //             break;
    //         case "Extra rapid cement":
    //             mastercrete();
    //             break;
    //         case "Snowcrete":
    //             snowcrete();
    //             break;
    //         case "Sulfacrete":
    //             mastercrete();
    //             break;
    //         case "Postcrete":
    //             postcrete();
    //             break;
    //         case "QAM":
    //             qam();
    //             break;
    //         case "Slablayer":
    //             slablayer();
    //             break;
    //         case "High strength concrete":
    //             highStrengthConcrete();
    //             break;
    //         case "MPC":
    //             highStrengthConcrete();
    //             break;
    //         case "Quickset concrete":
    //             quicksetConcrete();
    //             break;
    //         case "Repair concrete":
    //             repairConcrete();
    //             break;
    //         case "Hydralime":
    //             hydralime();
    //             break;
    //         default:
    //             console.log("No product was selected!");
    //
    //     }
    // });

    // jQuery("#product").change(function(){
    //     product =  jQuery("#product option:selected").val();
    //     // console.log("product selected was ", product);
    //
    //     switch (product) {
    //
    //         case "Mastercrete":
    //             displayMastercrete();
    //             break;
    //         case "GPC":
    //             displayMastercrete();
    //             break;
    //         case "Procem":
    //             displayMastercrete();
    //             break;
    //         case "Extra rapid cement":
    //             displayMastercrete();
    //             break;
    //         case "Snowcrete":
    //             displaySnowcrete();
    //             break;
    //         case "Sulfacrete":
    //             displayMastercrete();
    //             break;
    //         case "Postcrete":
    //             displayPostcrete();
    //             break;
    //         case "QAM":
    //             displayQam();
    //             break;
    //         case "Slablayer":
    //             displaySlablayer();
    //             break;
    //         case "High strength concrete":
    //             displayHighStrengthConcrete();
    //             break;
    //         case "MPC":
    //             displayHighStrengthConcrete();
    //             break;
    //         case "Quickset concrete":
    //             displayHighStrengthConcrete();
    //             break;
    //         case "Repair concrete":
    //             displayRepairConncrete();
    //             break;
    //         case "Hydralime":
    //             displayHydralime();
    //             break;
    //         default:
    //             console.log("No product was selected!");
    //
    //     }
    // });

    jQuery("#brick-type").change(function(){
        var typeOfBricks =  jQuery("#brick-type option:selected").val();

        if (typeOfBricks === "Single skin brick laying") {
            jQuery("#single-skin").show();
            jQuery("#repointing").hide();

        } else {
            jQuery("#single-skin").hide();
            jQuery("#repointing").show();
        }


    });

    function displayMastercrete(){
        jQuery("#mastercrete").show();
        jQuery("#postcrete").hide();
        jQuery("#slablayer").hide();
        jQuery("#qam").hide();
        jQuery("#hydralime").hide();
        jQuery("#highStrengthConcrete").hide();
        jQuery("#snowcrete").hide();
        jQuery("#repairConcrete").hide();
    }

    function displaySnowcrete(){
        jQuery("#snowcrete").show();
        jQuery("#mastercrete").hide();
        jQuery("#postcrete").hide();
        jQuery("#slablayer").hide();
        jQuery("#qam").hide();
        jQuery("#hydralime").hide();
        jQuery("#highStrengthConcrete").hide();
        jQuery("#repairConcrete").hide();
    }

    function displayQam(){
        jQuery("#mastercrete").hide();
        jQuery("#postcrete").hide();
        jQuery("#slablayer").hide();
        jQuery("#highStrengthConcrete").hide();
        jQuery("#hydralime").hide();
        jQuery("#qam").show();
        jQuery("#snowcrete").hide();
        jQuery("#repairConcrete").hide();
    }

    function displaySlablayer(){
        jQuery("#mastercrete").hide();
        jQuery("#postcrete").hide();
        jQuery("#qam").hide();
        jQuery("#highStrengthConcrete").hide();
        jQuery("#hydralime").hide();
        jQuery("#slablayer").show();
        jQuery("#snowcrete").hide();
        jQuery("#repairConcrete").hide();
    }

    function displayPostcrete(){
        jQuery("#cement-volume").text("");
        jQuery("#postcrete-bags").text("");
        jQuery("#water-litres").text("");
        jQuery("#mastercrete").hide();
        jQuery("#slablayer").hide();
        jQuery("#qam").hide();
        jQuery("#highStrengthConcrete").hide();
        jQuery("#hydralime").hide();
        jQuery("#postcrete").show();
        jQuery("#snowcrete").hide();
        jQuery("#repairConcrete").hide();
    }

    function displayHighStrengthConcrete(){

        jQuery("#mastercrete").hide();
        jQuery("#postcrete").hide();
        jQuery("#slablayer").hide();
        jQuery("#qam").hide();
        jQuery("#hydralime").hide();
        jQuery("#highStrengthConcrete").show();
        jQuery("#snowcrete").hide();
        jQuery("#repairConcrete").hide();
    }

    function displayRepairConncrete()
    {
        jQuery("#repairConcrete").show();
        jQuery("#mastercrete").hide();
        jQuery("#postcrete").hide();
        jQuery("#slablayer").hide();
        jQuery("#qam").hide();
        jQuery("#hydralime").hide();
        jQuery("#highStrengthConcrete").hide();
        jQuery("#snowcrete").hide();
    }


    function displayHydralime(){

        jQuery("#mastercrete").hide();
        jQuery("#postcrete").hide();
        jQuery("#slablayer").hide();
        jQuery("#qam").hide();
        jQuery("#highStrengthConcrete").hide();
        jQuery("#hydralime").show();
        jQuery("#snowcrete").hide();
        jQuery("#repairConcrete").hide();
    }

    function hydralime(){
        var length = parseFloat(jQuery("#larea-hydralime").val());
        var width = parseFloat(jQuery("#warea-hydralime").val());

        var totalArea = length * width / 10000;
        var cementBags = (totalArea * 0.3929).toFixed(1);
        var sandBags = (totalArea * 53.04).toFixed(1);
        var hydraBags = (totalArea * 0.1429).toFixed(1);


        jQuery("#cement-hydralime").text(cementBags);
        jQuery("#sand-hydralime").text(sandBags);
        jQuery("#hydra-hydralime").text(hydraBags);
        // jQuery(".calc-results").show();
        jQuery(".calc-results").css('visibility', 'visible');
    }

    function highStrengthConcrete(){


        var length = parseFloat(jQuery("#larea-hsc").val());
        var width = parseFloat(jQuery("#warea-hsc").val());
        var depth = parseFloat(jQuery("#darea-hsc").val());

        var totalVolume = length * width * depth / 1000000;
        var numberOfBags = totalVolume / 0.01 * 1.05;

        if(numberOfBags <= 0.5){
            numberOfBags = "Handy pack of 12.5Kg";
        } else{

            numberOfBags = Math.ceil(numberOfBags);
        }




        jQuery("#bags-hsc").text(numberOfBags);
        jQuery("#volume-hsc").text(totalVolume);
        // jQuery(".calc-results").show();
        jQuery(".calc-results").css('visibility', 'visible');
    }

    function quicksetConcrete(){


        var length = parseFloat(jQuery("#larea-hsc").val());
        var width = parseFloat(jQuery("#warea-hsc").val());
        var depth = parseFloat(jQuery("#darea-hsc").val());

        var totalVolume = length * width * depth / 1000000;
        var numberOfBags = Math.ceil(totalVolume / 0.0125 * 1.05);

        jQuery("#bags-hsc").text(numberOfBags);
        jQuery("#volume-hsc").text(totalVolume);
        // jQuery(".calc-results").show();
        jQuery(".calc-results").css('visibility', 'visible');
    }

    function repairConcrete()
    {
        var length = parseFloat(jQuery("#repair-larea-hsc").val());
        var width  = parseFloat(jQuery("#repair-warea-hsc").val());
        var depth  = parseFloat(jQuery("#repair-darea-hsc").val());

        var totalVolume = length * width * depth / 1000000 / 0.0125 * 1.05;

        console.log(totalVolume);

        var numberOfBags = (Math.ceil(totalVolume * 10) /10).toFixed(1) * 8;

        console.log(numberOfBags);

        jQuery("#repair-concrete-bags-hsc").text(numberOfBags);
        // jQuery("#volume-hsc").text(totalVolume);

        // jQuery(".calc-results").show();
        jQuery(".calc-results").css('visibility', 'visible');
    }

    function slablayer(){
        var length = parseFloat(jQuery("#slablayer-length").val());
        var width = parseFloat(jQuery("#slablayer-width").val());

        var totalArea = length * width;
        totalArea = totalArea.toFixed(2);

        var numberOfBags = totalArea / 0.4225;
        numberOfBags = Math.ceil(numberOfBags);

        jQuery("#numberOfBags-slablayer").text(numberOfBags);
        jQuery("#total-area-slablayer").text(totalArea);

        // jQuery(".calc-results").show();
        jQuery(".calc-results").css('visibility', 'visible');
    }

    function qam(){
        var length = parseFloat(jQuery("#qam-length").val());
        var width = parseFloat(jQuery("#qam-width").val());
        var metersWithOneBag, numberOfBags, numberOfBricks;
        var areaInSquareMeters = width * length;
        jQuery("#total-area-single").text(areaInSquareMeters);

        if (brickType === "Single skin") {


            numberOfBricks = length * width *64;
            numberOfBags = Math.ceil(numberOfBricks / 1750 * 75);

            jQuery("#numberOfBricks").text(Math.ceil(numberOfBricks));
            jQuery("#numberOfBags").text(numberOfBags);
            jQuery("#total-area-single").text(areaInSquareMeters);
            jQuery(".calc-results").css('visibility', 'visible');


        } else {
            jQuery(".repointing-input").show();
            jQuery(".single-skin-input").hide();
            var width = jQuery("#jointWidth").val();//A
            var depth = jQuery("#jointDepth").val();//B
            var totalArea = width * depth;
            totalArea = totalArea.toFixed(2)
            metersWithOneBag = ((1/75)/((width/100)*(depth/100)*(0.01)*100)).toFixed(2);

            jQuery("#meters-one-bag").text(metersWithOneBag);
            jQuery("#total-area-repointing").text(totalArea);
            jQuery(".calc-results").css('visibility', 'visible');
        }

    }
    function mastercrete() {

        var areaLength = parseFloat(jQuery("#larea").val());
        var areaWidth = parseFloat(jQuery("#warea").val());
        var areaDepth = parseFloat(jQuery("#darea").val());
        var bagsNo;
        var sandBallast;
        var area;
        var volume;
        var materialType;
        if (mixtureType === "Concrete 1:4"){

            // Calculate volume in cubic meters
            volume = (areaLength * areaWidth * areaDepth) / 1000000;

            // Calculate the number of cement bags, each bag is 25Kg.

            bagsNo = volume * 14;


            if (bagsNo <= 0.5){
                bagsNo = "Handy pack of 12.5Kg";
            } else {
                bagsNo = Math.ceil(bagsNo);
            }



            // Calculate the amount of Ballast in Kg
            sandBallast = (volume * 1850).toFixed(2);

            // jQuery("#material-type").text("Sand");
            materialType = "Ballast";


        } else {
            // Calculate area in square meters

            area = (areaLength * areaWidth) / 10000;
            bagsNo = area * 0.3929;
            if (bagsNo <= 0.5){
                bagsNo = "1 Handy pack of 12.5Kg";
            } else {
                bagsNo = Math.ceil(bagsNo);
            }
            // bagsNo = area * 0.3929;
            sandBallast = (area * 53.04).toFixed(2);
            // jQuery("#material-type").text("Ballast");
            materialType = "Sand";

        }

        if (bagsNo <= 0.5){
            bagsNo = "1 Handy Pack of 12.5Kg";
        }

        jQuery("#bagNo-label").text("Number of bags of cement:");
        jQuery("#bags-number").text(bagsNo);
        jQuery("#weight-label").text("Amount of " + materialType + " in Kg:");
        jQuery("#sand-ballast").text(sandBallast);
        jQuery("#extra-info").text("Add water till desired consistency.");

        // jQuery(".calc-results").show();
        jQuery(".calc-results").css('visibility', 'visible');
    }


    function snowcrete(){

        var areaLength = parseFloat(jQuery("#larea-snowcrete").val());
        var areaWidth = parseFloat(jQuery("#warea-snowcrete").val());
        var bagsNo;
        var sand;
        var area;
        var volume;
        if (snowcreteMixtureType === "Render 1:6 10mm thick"){
            // Calculate volume in cubic meters
            volume = (areaLength * areaWidth) * 1 / 1000000;

            // Calculate the number of cement bags
            bagsNo = (volume * 10) *25 + "kg";

            // Calculate the amount of Ballast in Kg
            sand = volume * 10 * 25 * 10.8;

        } else {
            // Calculate area in square meters

            area = (areaLength * areaWidth) / 10000;
            // bagsNo = Math.ceil(area * 0.3929);
            bagsNo = area * 0.3929;
            if (bagsNo <= 0.5){
                bagsNo = "Handy pack of 12.5Kg";
            } else {
                bagsNo = Math.ceil(bagsNo);
            }
            sand = area * 53.04;

        }

        jQuery("#snowcrete-bagNo").text(bagsNo);
        jQuery("#snowcrete-sand").text(Math.round(sand));
        jQuery("#extra-info-snowcrete").text("Add water till desired consistency.");


        jQuery(".calc-results").css('visibility', 'visible');
    }



    function postcrete() {
        var postWidth;//A
        var postLength;//B
        var numberOfPosts;//C
        var volumeOfCement;
        var postcreteBagsNo;
        var litersOfWater;

        postLength = parseFloat(jQuery("#plength").val());
        postWidth = parseFloat(jQuery("#pwidth").val());
        numberOfPosts = parseFloat(jQuery("#nposts").val());

        volumeOfCement = postLength * 0.25 * postWidth * postWidth * 9 / 1000000;

        postcreteBagsNo = ((volumeOfCement - postLength * 0.25 * postWidth * postWidth / 1000000) * numberOfPosts / 0.012) * 0.5;

        if (postcreteBagsNo <= 0.5) {
            postcreteBagsNo = "Handy pack of 12.5Kg";
        } else {
            postcreteBagsNo = postcreteBagsNo.toFixed(1);
        }

        // postcreteBagsNo = (postLength * 0.25 * postWidth * postWidth * 9 / 1000000 - postLength * 0.25 * postWidth * postWidth) * numberOfPosts / 0.012 * 0.5;
        // litersOfWater = (postLength*0.25*postWidth*postWidth*9/1000000-postLength*0.25*postWidth*postWidth)*numberOfPosts/6*1000;
        litersOfWater = (volumeOfCement - postLength * 0.25 * postWidth * postWidth / 1000000) / 6 * 1000;
        litersOfWater = litersOfWater.toFixed(1);

        jQuery("#postcrete-bags").text(postcreteBagsNo);
        jQuery("#water-litres").text(litersOfWater);

        // jQuery(".calc-results").show();
        jQuery(".calc-results").css('visibility', 'visible');

    }
});
