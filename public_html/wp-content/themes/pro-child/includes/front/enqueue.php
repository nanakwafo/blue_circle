<?php

function telp_enqueue()
{
    $uri = get_theme_file_uri();

    // Register Styles
    wp_register_style( 'telp_slick_slider', $uri . '/assets/slick-slider/slick.css' );
    wp_register_style( 'bootstrap_css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' );
//    wp_register_style( 'telp_product_calculator', $uri . '/assets/calculator/calculator.css' );

    // Enqueue Styles
    wp_enqueue_style( 'telp_slick_slider' );
    wp_enqueue_style( 'bootstrap_css' );
    wp_enqueue_style( 'telp_product_calculator' );

    // Register Scripts
    wp_register_script( 'telp_slick_slider_scripts', $uri . '/assets/slick-slider/slick.min.js' );
    wp_register_script( 'boostrap_js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js' );
    wp_register_script( 'telp_product_calculator_scripts', $uri . '/assets/calculator/calculator.js', false, false, true);
    wp_register_script( 'app_js', $uri . '/assets/app.js', 'jquery', '1.0.0', true);

    // Enqueue Scripts
    wp_enqueue_script( 'telp_slick_slider_scripts' );
    wp_enqueue_script( 'boostrap_js' );
    wp_enqueue_script( 'telp_product_calculator_scripts' );
    wp_enqueue_script( 'app_js' );
}
