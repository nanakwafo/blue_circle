<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	?>
	
	
	
	<?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="search-wrap">
					
      
                    <header class="entry-header">
                      <h2><?php the_title(); ?></h2>
                    </header>
                    <div class="search-content">
                    <?php the_field('search_excerpt'); ?>
                    </div>
                        
                    </div>
                    
                    <div class="search-result-view">
                    <a href="<?php the_permalink(); ?>"><img src="/wp-content/uploads/2020/01/arrow@2x.png" /></a>
                    </div>
                </article>
		<?php
	}
	?>
	
	<?php
}

?>